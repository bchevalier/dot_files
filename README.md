# dot_files

**dot_files** is a collection of mostly hidden files (typically residing in a user's home directory) used to configure applications such as vim and tmux

---

## Installation

Run the following commands:
```
$ cd ~
$ git clone git@gitlab.com:bchevalier/dot_files.git
$ ln -s dot_files/tmux/dot_tmux.conf .tmux.conf
```
